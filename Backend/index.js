const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const {Client} = require('pg')

const app = express();

app.use(bodyParser.json())

app.use(cors({
  origin: 'http://localhost:5173',
})
)

const client = new Client({
  host: "localhost",
  user: "postgres",
  port: 5432,
  password: 'root',
  database: 'SignInCred'
})

client.connect();


app.get("/api", (request, response) => {
  response.send("<h1>Welcome to my API<h1>")
})

app.post("/api/signup", (request, response) => {
  const credentials = request.body;
  client.query(`INSERT INTO userdetails (email, username, password, logintime) VALUES('${credentials.email}', '${credentials.username}', '${credentials.password}', 1)`, (err, result) => {
    if (!err) {
      console.log("INSERTED");
      response.status(200).json({ message: "User successfully registered" });
    }
    else {
      console.log(err);
      response.status(500).json({ error: "Internal Server Error" });
    }
  })
})

app.post("/api/signin", (request, response) => {
  const credentials = request.body;
  client.query(`SELECT email, password FROM userdetails WHERE email='${credentials.email}'`, (err, result) => {
    if (!err) {
      if (result.rows[0].email === credentials.email && result.rows[0].password === credentials.password) {
        response.status(200).json({ message: "Password and email is correct" });
      } else {
        response.status(403 ).json({ message: "Password and email is incorrect" });
      }
    } else {
      console.log(err);
      response.status(500).json({ error: "Internal Server Error" });
    }
  })
})

app.listen(5000, () => {
  console.log("Running on http://localhost:5000");
})