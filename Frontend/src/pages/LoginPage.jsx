import { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import "./styles.css";

function LoginPage() {
  const navigate = useNavigate();
  const [credentials, setCredentials] = useState({ email: "", password: "" });
  const handleChange = (e) => {
    setCredentials({
      ...credentials,
      [e.target.name]: e.target.value,
    });
  };

  const sendCred = (e) => {
    e.preventDefault();
    fetch("http://localhost:5000/api/signin", {
      method: "post",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(credentials),
    }).then((response) => {
      if (response.ok) {
        navigate("/home");
      }
    });
    // console.log("Working");
  };

  return (
    <>
      <div className="main-container sign-in">
        <div id="heading">Sign In</div>
        <form id="form" name="form" onSubmit={sendCred}>
          <section>
            <label htmlFor="email">Email</label>
            <input
              id="email"
              name="email"
              type="email"
              onChange={handleChange}
              autoFocus
              required
            />
          </section>
          <section>
            <label htmlFor="password">Password</label>
            <input
              id="password"
              name="password"
              type="password"
              onChange={handleChange}
              required
            />
          </section>
          <button>Sign in</button>
        </form>
        <div id="sign-up-link">
          <Link to="/signup">Create New account</Link>
        </div>
      </div>
    </>
  );
}

export default LoginPage;
