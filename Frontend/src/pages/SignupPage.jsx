import React, { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import "./styles.css";

const SignupPage = () => {
  const navigate = useNavigate();
  const [credentials, setCredentials] = useState({
    email: "",
    username: "",
    password: "",
  });

  //populates the credentials object
  const handleChange = (e) => {
    setCredentials({
      ...credentials,
      [e.target.name]: e.target.value,
    });
  };
  console.log(credentials);

  // Sends credentials for validation and storing
  const sendCred = (e) => {
    e.preventDefault();
    fetch("http://localhost:5000/api/signup", {
      method: "post",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(credentials),
    }).then((response) => {
      if (response.ok) {
        navigate("/home");
      }
    });
    // console.log("Working");
  };

  return (
    <>
      <div className="main-container sign-up">
        <div id="heading">Sign Up</div>
        <form id="form" name="form" onSubmit={sendCred}>
          <section>
            <label htmlFor="email">Email</label>
            <input
              id="email"
              name="email"
              type="email"
              onChange={handleChange}
              autoFocus
              required
            />
          </section>
          <section>
            <label htmlFor="username">Username</label>
            <input
              id="username"
              name="username"
              type="text"
              onChange={handleChange}
              required
            />
          </section>
          <section>
            <label htmlFor="password">Password</label>
            <input
              id="password"
              name="password"
              type="password"
              onChange={handleChange}
              required
            />
          </section>

          <button>Sign Up</button>
        </form>
        <div id="link">
          <Link to="/">Already have an account?</Link>
        </div>
      </div>
    </>
  );
};

export default SignupPage;
